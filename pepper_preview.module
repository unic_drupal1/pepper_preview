<?php

/**
 * @file
 * pepper_preview.module
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Site\Settings;
use \Drupal\node\Entity\Node;
/**
 * Implements form_alter_HOOK() for some major form changes.
 */
function pepper_preview_form_alter(&$form, $form_state, $form_id) {
  // Are we on an edit form?
  if (_pepper_preview_is_content_form($form, $form_state, $form_id)) {
    // Action buttons.
    if (isset($form['actions'])) {
      $form['actions']['preview']['#submit'][] = 'pepper_preview__callback';
      // Set triggering element to value expected by token module to prevent it
      // from creating a menu link entry for this preview.
      // See token_node_menu_link_submit()
      $form['actions']['preview']['#id'] = 'edit-preview';
    }
  }
}

/**
 * Custom Preview Callback method.
 */
function pepper_preview__callback(array $form, FormStateInterface $form_state) {
  // Get Entity Id.
  $entity = $form_state->getformObject()->getEntity();
  $uuid = $entity->uuid();

  /** @var \Drupal\pepper_preview\Service\PreviewHelper $previewHelper */
  $previewHelper = Drupal::service('pepper_preview.preview_helper');
  $previewKey = $previewHelper->getTempstoreKey($entity);

  /** @var \Drupal\Core\TempStore\SharedTempStore $tempstore */
  $tempstore = $previewHelper->getTempstore();
  $originalTempstore = $previewHelper->getOriginalTempstore();


  // Inject siblings into paragraphs, cause in preview it could be the case, that the parent node is also not yet saved.
  // In that case it would not be possible to do all the layout stuff, if the entities themselves don't know about the
  // other paragraphs.
  if($entity->hasField('field_layout_paragraphs')) {
    $siblings = [];
    // Collect all siblings.
    foreach ($entity->field_layout_paragraphs->getValue() as $index => $val) {
      if (!empty($val['entity']) && $val['entity'] instanceof EntityInterface) {
        $siblings[$val['entity']->uuid()] = $val['entity'];
      }
    }
    // Pass the siblings to the paragraphs entities.
    if ($siblings) {
      foreach ($entity->field_layout_paragraphs->getValue() as $index => $val) {
        if (!empty($val['entity']) && $val['entity'] instanceof EntityInterface) {
          // Remove the entity itself from the siblings list.
          // If we don't do that, only the first entity will get its serialized siblings, all the other siblings become
          // NULL. This seems to be a bug/feature in the php serialize function. But did not find any issue by googling
          // for it. But it looks like identical sub arrays/objects, attached multiple times, get lost in that case here.
          $paragraphSiblings = array_diff_key($siblings, array_flip([$val['entity']->uuid()]));
          $val['entity']->siblings = $paragraphSiblings;
        }
      }
    }
  }

  // Serialize the entity.
  $serialized = serialize($entity);
  $tempstore->set($previewKey, $serialized);
  // Store it in the original temp store, which NodeForm uses, too.
  $originalTempstore->set($previewKey, $form_state);

  // Get the base url for the preview system via settings in ENV.settings.local.php.
  $previewBaseUrl = Settings::get('pepper_preview.base_url', 'http://localhost:3333');

  // Check if the site is multilingual
  // If so, insert the language prefix into the preview url.
  /** @var \Drupal\Core\Language\LanguageManager $languageManager */
  $languageManager = \Drupal::service('language_manager');
  if ($languageManager->isMultilingual() && $entity instanceof Node && !empty($entity->get('langcode')->value)) {
    $path = $previewBaseUrl . '/' . $entity->get('langcode')->value . '/preview/' . $previewKey;
  }
  else {
    $path = $previewBaseUrl . '/preview/' . $previewKey;
  }

  // Attach timestamp as query parameter to prevent from being cached in varnish or somewhere else.
  $path .= '?t=' . Drupal::time()->getRequestTime();

  $response = new TrustedRedirectResponse($path);
  $form_state->setResponse($response);
}

/**
 * Check if were on a content edit form.
 */
function _pepper_preview_is_content_form($form = NULL, $form_state = NULL, $form_id = NULL) {
  $is_content_form = FALSE;

  // Get route name.
  $route_name = \Drupal::routeMatch()->getRouteName();

  // Routes to include.
  $route_names = [
    'node.add',
    'entity.node.content_translation_add',
    'quick_node_clone.node.quick_clone',
    'entity.node.edit_form',
  ];

  if (
    in_array($route_name, $route_names, TRUE) ||
    ($form_state && ($form_state->getBuildInfo()['base_form_id'] ?? NULL) === 'node_form') ||
    ($route_name === 'entity.group_content.create_form' && strpos($form_id, 'group_node') === FALSE)
  ) {
    $is_content_form = TRUE;
  }

  // Forms to exclude.
  // If media library widget, don't use new content edit form.
  // gin_preprocess_html is not triggered here, so checking
  // the form id is enough.
  $form_ids_to_ignore = [
    'media_library_add_form_',
    'views_form_media_library_widget_',
    'views_exposed_form',
    'date_recur_modular_sierra_occurrences_modal',
  ];

  foreach ($form_ids_to_ignore as $form_id_to_ignore) {
    if ($form_id && strpos($form_id, $form_id_to_ignore) !== FALSE) {
      $is_content_form = FALSE;
    }
  }

  return $is_content_form;
}

