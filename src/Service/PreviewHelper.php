<?php

namespace Drupal\pepper_preview\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\TempStore\SharedTempStoreFactory;

class PreviewHelper {

  /**
   * @var \Drupal\Core\TempStore\SharedTempStore
   */
  protected $tempstore;

  /**
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $originalTempStore;

  public function __construct(SharedTempStoreFactory $tempstoreFactory, PrivateTempStoreFactory $privateTempstoreFactory) {
    $this->tempstore = $tempstoreFactory->get('decoupled_preview');
    $this->originalTempStore = $privateTempstoreFactory->get('node_preview');
  }

  public function getTempstore() {
    return $this->tempstore;
  }

  public function getOriginalTempstore() {
    return $this->originalTempStore;
  }

  public function getTempstoreKey(EntityInterface $entity) {
    return $entity->uuid() . '__' . $entity->language()->getId();
  }


}
