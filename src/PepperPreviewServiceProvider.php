<?php

namespace Drupal\pepper_preview;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\Site\Settings;

class PepperPreviewServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $parameters = $container->getParameter('session.storage.options');

    $cookie_domain_override = Settings::get('pepper_preview.cookie_domain_override', '');
    if (!empty($cookie_domain_override)) {
      //set cookie domain to override the default
      $parameters['cookie_domain'] = $cookie_domain_override;
      $container->setParameter('session.storage.options', $parameters);
    }

    $cors_config = $container->getParameter('cors.config');
    $cors_allowed_origins = Settings::get('pepper_preview.cors_settings.allowed_origins_override', '');

    // Override cors settings with drupal settings.
    if (!empty($cors_allowed_origins)) {
      $cors_config['allowedOrigins'] = $cors_allowed_origins;
    }

    // Override cors supported credentials with drupal settings.
    $cors_supports_credentials = Settings::get('pepper_preview.cors_settings.supports_credentials_override', '');
    if (!empty($cors_supports_credentials)) {
      $cors_config['supportsCredentials'] = $cors_supports_credentials;
    }

    // Set cors settings to allow credentials and override allowed origins
    $container->setParameter('cors.config', $cors_config);

  }

}
